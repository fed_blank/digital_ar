﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Music : MonoBehaviour
{
    #region Private Fields

    private static AudioSource _audioSource;

    #endregion

    #region Unity Methods

    private void Start()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    #endregion

    #region PublicMethods

    public static void PlayAudio(AudioClip audioClip)
    {
        Debug.LogError(audioClip.name);
        _audioSource.PlayOneShot(audioClip);
    }

    public static void StopAllAudio()
    {
        if (_audioSource.isPlaying)
        {
            _audioSource.Stop();
            _audioSource.clip = null;
            Debug.LogError("stop");
        }
    }
    #endregion
}
