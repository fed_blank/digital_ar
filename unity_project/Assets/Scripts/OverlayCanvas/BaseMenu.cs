﻿using UnityEngine;
using UnityEngine.UI;

public class BaseMenu : PageUI
{
    #region Properties

    public Button NextButton
    {
        get
        {
            return _nextButton;
        }
        set
        {
            _nextButton = value;
        }
    }

    #endregion


    #region UnityMethods

    [SerializeField] private Button _menuButton;
    [SerializeField] private Button _nextButton;

    #endregion

    #region Unity Methods

    private void Awake()
    {
        _menuButton.onClick.AddListener(() =>
        {

        });
    }

    #endregion


}
