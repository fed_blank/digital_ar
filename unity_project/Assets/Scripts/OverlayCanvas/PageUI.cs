﻿using UnityEngine;

public class PageUI : MonoBehaviour
{
    #region Unity Editor

    [SerializeField] private GameObject _currentPage;

    #endregion

    #region Methods

    public virtual void ShowPage()
    {
        _currentPage.SetActive(true);
    }

    public virtual void HidePage()
    {
        _currentPage.SetActive(false);
    }

    #endregion
}
