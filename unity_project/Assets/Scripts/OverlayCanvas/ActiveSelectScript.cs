﻿using UnityEngine;
using UnityEngine.UI;

public class ActiveSelectScript : MonoBehaviour
{

    #region Unity Editor

    [SerializeField] private Fines _fines;
    [SerializeField] private MainMenu _mainMenu;

    #endregion

    #region Private Fields

    private Button _currentButton;

    #endregion

    #region Unity Methods

  /*  private void Awake()
    {
        _currentButton.onClick.AddListener(() =>
        {
            _mainMenu.HidePage();
            _fines.Show();
        });
    }
    */
    private void Start()
    {
        _currentButton = GetComponent<Button>();
        _currentButton.onClick.AddListener(() =>
        {
            _mainMenu.HidePage();
            _fines.Show();
        });
    }

    #endregion
}
