﻿using UnityEngine;
using UnityEngine.UI;

public class MainMenu : PageUI
{
    #region Unity Editor

    [SerializeField] private Button _quitButton;
    [SerializeField] private GameObject _baseMenu;

    #endregion

    #region Unity Methods

    private void Awake()
    {
        _quitButton.onClick.AddListener(AppQuitController.QuitApp);
    }

    #endregion

    #region Overrides Methods

    public override void HidePage()
    {
        base.HidePage();
        _baseMenu.SetActive(true);
    }

    #endregion
}
