﻿using UnityEngine;
using UnityEngine.UI;

public class ConfirmationPopup :PageUI
{
    #region Unity Editor

    [SerializeField] private Button _agreeButton;
    [SerializeField] private Button _notAgreeButton;
    [SerializeField] private Text _descriptionText;

    #endregion

    #region Unity Methods

    private void Awake()
    {
        _agreeButton.onClick.AddListener(() =>
        {

        });
        _notAgreeButton.onClick.AddListener(() =>
        {

        });
    }

    #endregion

    #region Methods

    public void SetText(string textDesc)
    {
        _descriptionText.text = textDesc;
    }

    #endregion
}
