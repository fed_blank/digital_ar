﻿using UnityEngine;

public class AppQuitController : MonoBehaviour
{
    #region Public Methods

    public static void QuitApp()
    {
        Application.Quit();
    }

    #endregion
}
