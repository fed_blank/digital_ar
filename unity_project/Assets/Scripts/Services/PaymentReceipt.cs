﻿using UnityEngine;
using UnityEngine.UI;

public class PaymentReceipt : MonoBehaviour
{
    #region Unity Editor

    [SerializeField] private BaseMenu _baseMenu;
    [SerializeField] private GameObject _createReceiptTarget;
    [SerializeField] private GameObject _downloadReceiptTarget;
    [SerializeField] private AudioClip _downloadReceiptClip;

    #endregion

    #region Private Fields

    private bool isActive = false;

    #endregion

    #region Unity Methods

    private void Awake()
    {
        _baseMenu.NextButton.onClick.AddListener(() =>
        {
            if (isActive)
            {
                Hide();
            }
        });
    }

    #endregion

    #region Public Methods

    public void Show()
    {
        ActiveDownloadReceiptPage();
        isActive = true;
    }

    private void ActiveDownloadReceiptPage()
    {
        _createReceiptTarget.SetActive(false);
        _downloadReceiptTarget.SetActive(true);
        Music.PlayAudio(_downloadReceiptClip);
    }

    public void Hide()
    {
        _createReceiptTarget.SetActive(false);
        _downloadReceiptTarget.SetActive(false);
        Music.StopAllAudio();
        isActive = false;
    }

    #endregion
}
