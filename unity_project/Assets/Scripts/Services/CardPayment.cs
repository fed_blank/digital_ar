﻿using System.Collections;
using UnityEngine;

public class CardPayment : MonoBehaviour
{
    #region Constants

    const float TIME_FOR_PAUSE_BEFORE_ACTIVE_CARD_DETAILS_TARGET = 2f;

    #endregion

    #region Unity Editor

    [SerializeField] private BaseMenu _baseMenu;
    //[SerializeField] private GameObject _cards;
    [SerializeField] private GameObject _cardTarget;
    [SerializeField] private GameObject _payTarget;
    [SerializeField] private AudioClip _cardDetailsClip;
    [SerializeField] private AudioClip _authClip;
    [SerializeField] private AudioClip _checkClip;
    [SerializeField] private AudioClip _secondPaymentClip;
    [SerializeField] private AudioClip _thirdPaymentClip;
    [SerializeField] private AudioClip _fourghPaymentClip;
    #endregion 

    #region Private Fields

    private int _stateIndex;
    private bool _isActive = false;

    #endregion

    #region Unity Methods

    private void Awake()
    {

        _baseMenu.NextButton.onClick.AddListener(() =>
        {
            if (_isActive)
            {
                switch (_stateIndex)
                {
                    case 0:
                        ActivePaymentPage();
                        _stateIndex++;
                        break;
                    case 1:
                        Music.StopAllAudio();
                        Music.PlayAudio(_secondPaymentClip);
                        _stateIndex++;
                        break;
                    case 2:
                        Music.StopAllAudio();
                        Music.PlayAudio(_thirdPaymentClip);
                        _stateIndex++;
                        break;
                    case 3:
                        Music.StopAllAudio();
                        Music.PlayAudio(_fourghPaymentClip);
                        _stateIndex++;
                        break;
                    case 4:
                        ActiveAuthentication();
                        _stateIndex++;
                        break;
                    case 5:
                        ActiveCheckPage();
                        _stateIndex++;
                        break;
                    case 6:
                        Hide();
                        break;
                }
            }
        });
    }

    #endregion

    #region Public Methods

    public void Show()
    {
        ActiveCardDetailsPage();
        _stateIndex = 0;
        _isActive = true;
    }

    public void Hide()
    {
        _cardTarget.SetActive(false);
        _payTarget.SetActive(false);
        Music.StopAllAudio();
        _isActive = false;
    }

    #endregion

    #region Private Methods

    private void ActiveCardDetailsPage()
    {
        _cardTarget.SetActive(false);
        _payTarget.SetActive(false);
        Music.PlayAudio(_cardDetailsClip);
        StartCoroutine(WaitPauseForActiveTarget());
    }

    private void ActivePaymentPage()
    {
        Music.StopAllAudio();
        _payTarget.SetActive(true);
        _cardTarget.SetActive(false);
    }

    private void ActiveAuthentication()
    {
        _cardTarget.SetActive(false);
        _payTarget.SetActive(false);
        Music.PlayAudio(_authClip);
    }

    private void ActiveCheckPage()
    {
        Music.StopAllAudio();
        Music.PlayAudio(_checkClip);
    }

    private IEnumerator WaitPauseForActiveTarget()
    {
        yield return new WaitForSeconds(TIME_FOR_PAUSE_BEFORE_ACTIVE_CARD_DETAILS_TARGET);
        _cardTarget.SetActive(true);
    }

    #endregion
}
