﻿using UnityEngine;
using UnityEngine.UI;

public class Payment : MonoBehaviour
{

    #region Unity Editor

    [SerializeField] private BaseMenu _baseMenu;

    [SerializeField] private PaymentReceipt _paymentReceipt;
    [SerializeField] private CardPayment _cardPayment;
    [Header("Buttons")]
    [SerializeField] private Button _receiptButton;
    [SerializeField] private Button _cardButton;
    [Header("Targets")]
    [SerializeField] private GameObject _cardTarget;
    [SerializeField] private GameObject _receiptTarget;
    [Header("Clips")]
    [SerializeField] private AudioClip _selectReceiptOrCardAudioClip;

    #endregion

    #region Unity Methods

    private void Awake()
    {
        _receiptButton.onClick.AddListener(() =>
        {
            Hide();
            _paymentReceipt.Show();
        });
        _cardButton.onClick.AddListener(() =>
        {
            Hide();
            _cardPayment.Show();
        });
    }

    #endregion

    #region Public Methods

    public void Show()
    {
        ShowDataInputOrReceiptGeneretion();
        Music.PlayAudio(_selectReceiptOrCardAudioClip);
        _receiptButton.gameObject.SetActive(true);
        _cardButton.gameObject.SetActive(true);
        _baseMenu.NextButton.gameObject.SetActive(false);
    }

    #endregion

    #region Private Methods

    private void ShowDataInputOrReceiptGeneretion()
    {
        _cardTarget.SetActive(true);
        _receiptTarget.SetActive(true);
    }

    private void Hide()
    {
        _receiptButton.gameObject.SetActive(false);
        _cardButton.gameObject.SetActive(false);
        _cardTarget.SetActive(false);
        _receiptTarget.SetActive(false);
        Music.StopAllAudio();
        _baseMenu.NextButton.gameObject.SetActive(true);
    }

    #endregion
}
