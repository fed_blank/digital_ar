﻿using UnityEngine;
using UnityEngine.UI;

public class Fines : MonoBehaviour
{

    #region Unity Editor

    [SerializeField] private BaseMenu _baseMenu;
    [SerializeField] private GameObject _taxPenaltiesTarget;
    //[SerializeField] private GameObject _selectDebtTarget;
    [SerializeField] private AudioClip _baseSiteClip;
    //[SerializeField] private AudioClip _selectDebtClip;
    [SerializeField] private Payment _payment;
    [SerializeField] private AudioClip _secondBaseSiteAudioClip;

    #endregion

    #region Private Fields

    private int _stateIndex;
    private bool _isActive = false;

    #endregion

    #region Unity Methods

    private void Awake()
    {
        _baseMenu.NextButton.onClick.AddListener(() =>
        {
            if (_isActive)
            {
                Debug.LogError(_stateIndex);
                switch (_stateIndex)
                {
                    /*case 0:
                        Music.StopAllAudio();
                        ShowPaymentsOrPeni();
                        _stateIndex++;
                        break;*/
                    case 0:
                        Music.StopAllAudio();
                        Music.PlayAudio(_secondBaseSiteAudioClip);
                        _stateIndex++;
                        break;
                    case 1:
                        Hide();
                        Music.StopAllAudio();
                        _payment.Show();
                        break;
                }
            }
        });
    }

    #endregion

    #region Methods

    public void Show()
    {
        ShowBaseSite();
        _stateIndex = 0;
        _isActive = true;
    }

    private void ShowBaseSite()
    {
        _taxPenaltiesTarget.SetActive(true); // ввод данных карты (тригер)
        //_selectDebtTarget.SetActive(false); // выбор целевой задолженности
        Music.PlayAudio(_baseSiteClip);
    }

   /* private void ShowPaymentsOrPeni()
    {
        //_selectDebtTarget.SetActive(true);
        //_taxPenaltiesTarget.SetActive(false);
        Music.PlayAudio(_selectDebtClip);
    }*/

    public void Hide()
    {
        //_selectDebtTarget.SetActive(false);
        _taxPenaltiesTarget.SetActive(false);
        _isActive = false;
    }
    #endregion

}
